import os

import telebot
from dotenv import load_dotenv

load_dotenv()

bot = telebot.TeleBot(os.getenv('token'))
admin_id = int(os.getenv('admin_id'))


@bot.message_handler(commands=['start'])
def start_message(message):
    if message.chat.id == admin_id:
        bot.send_message(message.chat.id, 'Привет. Ждём, когда отправят видео или фото.')
    else:
        bot.send_message(
            message.chat.id,
            '''Привет! Отправьте фото или видео, 
            перед публикацией в нашу базу данных 
            оно будет проверено нашим администратором.'''
        )


@bot.message_handler(
    content_types=[
        'photo',
        'video',
        'text',
        'audio',
        'document',
        'sticker',
        'video note',
        'voice',
        'location',
        'contact',
        'new_chat_members',
        'left_chat_member',
        'new_chat_title',
        'new_chat_photo',
        'delete_chat_photo',
        'group_chat_created',
        'supergroup_chat_created',
        'channel_chat_created',
        'migrate_to_chat_id',
        'migrate_from_chat_id',
        'pinned_message'
    ]
)
def photo_video_admin(msg):
    if msg.content_type == 'photo':
        bot.send_photo(admin_id, msg.photo[0].file_id)
        bot.send_message(msg.chat.id, 'Фото было отправлено администратору.')
    elif msg.content_type == 'video':
        bot.send_video(admin_id, msg.video.file_id)
        bot.send_message(msg.chat.id, 'Видео было отправлено администратору.')
    else:
        bot.send_message(
            msg.chat.id, '''Это не видео и не фото :(
                         'Ждём, когда отправят видео или фото.'''
        )


bot.polling()
